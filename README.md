# DEPRECATED Repository

See https://www.npmjs.com/package/drowl-sass-base instead

# webksdct Admin

This repo holds special administration stylesheets for webksdct.

**Important:** webksdct itself is NOT a Drupal Backend theme. This stylesheets adresses very specific administrative interfaces like Drupals Layout Builder or the Entity Reference Layout module.

# Versioning
We use branches for the major drupal version and tags for the minor repository versions.

So the logic is:
[Drupal Major Version].[Repository Version (breaking changes)].[Patches / Fixes / Improvements]

## Create a new release
To create a new release, you have to commit + push your changes. Afterwards you create a new tag
named by the new version (eg. 1.2.1, see the logic above).
Now sync the tags and gitlab ci will do the rest. You have to pull if ci created the new release to
do further changes.

# Deprecated Branches

* 1.0.0: Oldest branch, not sure what was the breaking change here
* 1.0.1: Before IEF module was removed as a dependency (https://www.drupal.org/project/entity_reference_layout/issues/3068270)

# Usage

Install as dependency via npm (not dev) and run 'gulp build-frontend-libraries' / 'gulp bfl' from the webksdct gulpfile. So now webksdct-admin is located under frontend_libraries/webksdct-admin.

So you just need to @import the files in your own, eg: modules/backend/layout_builder.scss. The files itself import a lot of styles from webksdct, to have proper preview styles etc., so if you change the folder names or something, this will fail.

# Layout Builder

The Layout builder ui doesn't use the configuread admin theme of the Drupal instance, it uses the frontend theme. So you just need to extend the "layout_builder/drupal.layout_builder" library with your own library - defined in your THEME.libraries.yml - in your THEME.info.yml:

**THEME.info.yml:**
```
libraries-extend:
  layout_builder/drupal.layout_builder:
    - webksdct/layout_builder
```

**THEME.libraries.yml:**
```
layout_builder:
  version: 1.0
  css:
    theme:
      css/modules/backend/layout_builder.min.css: {}
```


# ERL - Entity reference Layout module

ERL basically is an integrated layout builder inside the node form, to structure paragraphs using a drag and drop interface.

So, different to the regular layout builder, the admin theme is active and we just add stylesheets to it. To do so, our drowl_layouts module searches for a "CURRENT_ACTIVE_FRONTEND_THEME/drowl_layouts_erl_additions" library and attaches it to the ERL field widget.

So you just need to define it in your THEME.libraries.yml:

**THEME.libraries.yml:**
```
drowl_layouts_erl_additions:
  version: 1.0
  css:
    layout:
      css/modules/backend/entity_reference_layout.min.css: {}
```
